import React from 'react';
// import { Redirect } from 'react-router-dom';


// // // home routes
// import Dashboard from './containers/dashboard/index';

// authentication routes
import Auth from './containers/auth/index';
import Login from './containers/auth/login';
import Logout from './containers/auth/logout';
import Signup from './containers/auth/signup';
import ForgetPassword from './containers/auth/forget_password';
import ResetPassword from './containers/auth/reset_password'

// error route
import NotFound from './containers/error/notfound';

// home
import Home from './containers/home/index';

// // #################################################################

// // dashboard home routes
// import DashboardHome from './containers/home/dashboard_home';
// import ATM from './containers/atm/atm_list';
// import Report from './containers/report/report';




export const routes = [
    {
        path: '/dashboard',
        title: '',
        name: 'Dashboard',
        // exact: true,
        // private: true,
        private: false,
        component: Home,
    },
    {
        path: '/auth',
        title: 'auth',
        name: '',
        // exact: true,
        private: true,
        component: Auth,

    },
    {
        path: '/',
        title: 'Dashboard',
        name: '',
        exact: true,
        // private: true,
        private: false,
        component: Home,

    },
    {
        path: '/:anything',
        title: '404- Not Found',
        name: 'Not Found',
        component: NotFound
    }
]

export const authRoutes = [
    {
        path: '/auth/',
        title: 'Login',
        exact: true,
        name: 'Login',
        component: Login,
    },
    {
        path: '/auth/login',
        title: 'Login',
        exact: true,
        name: 'Login',
        component: Login,
    },
    {
        path: '/auth/logout',
        title: 'Logout',
        exact: true,
        name: 'Logout',
        component: Logout
    },
    {
        path: '/auth/signup',
        title: 'SignUp',
        exact: true,
        name: 'SignUp',
        component: Signup
    },
    {
        path: '/auth/reset-password',
        title: 'Reset Password',
        exact: true,
        name: 'Reset Password',
        component: ForgetPassword,
    },
    {
        path: '/auth/forget_password',
        title: 'Forget Password',
        exact: true,
        name: 'Forget Password',
        component: ResetPassword
    },
]

// export const dashboardRoutes = [
//     {
//         path: '/',
//         title: 'Dashboard',
//         exact: true,
//         name: '',
//         component: DashboardHome,
//     },
//     {
//         path: '/dashboard',
//         title: 'Dashboard',
//         exact: true,
//         name: '',
//         component: DashboardHome,
//     },
//     {
//         path: '/dashboard/atm',
//         title: 'ATM',
//         exact: true,
//         name: '',
//         component: ATM,
//     },
//     {
//         path: '/dashboard/report',
//         title: 'Report',
//         exact: true,
//         name: '',
//         component: Report,
//     },
// ]