import {LIST_CLIENT, LIST_CLIENT_ERROR, LIST_CLIENT_SUCCESS, CREATE_CLIENT, CREATE_CLIENT_ERROR, CREATE_CLIENT_SUCCESS, UPDATE_CLIENT, UPDATE_CLIENT_ERROR, UPDATE_CLIENT_SUCCESS, DELETE_CLIENT, DELETE_CLIENT_ERROR, DELETE_CLIENT_SUCCESS} from "../constants/action_constants";
import LModel from "../services/api";
import AuthService from "../services/auth_service";


/*

List Clinet & Filter Client
Create Client
Detail Client *
Update Client
Flag as Delete

*/


export const listClient = (dispatch) => (id, filter) => {
    
    dispatch({
        type: LIST_CLIENT,
        payload: {
            data: "data",
            loading: true
        }
    })
    
    LModel.find('clients/',id, filter)
        .then(response => {
            
            // Create Client Service handle the response
            
            setTimeout(() => {
                dispatch({
                    type: LIST_CLIENT_SUCCESS,
                    payload: {
                        loading: false,
                        data: response
                    }
                })
            }, 2000)
            // save data
        })
        .catch(error => {
            dispatch({
                type: LIST_CLIENT_ERROR,
                payload: {
                    error: error,
                    loading: false
                }
            })
        })
}

export const createClient = (dispatch) => (data) => {
    
    dispatch({
        type: CREATE_CLIENT,
        payload: {
            data: data,
            loading: true
        }
    })
    
    LModel.create('clients/', data)
        .then(response => {

            // Create Client Service handle the response


            setTimeout(() => {
                dispatch({
                    type: CREATE_CLIENT_SUCCESS,
                    payload: {
                        loading: false,
                        data: response
                    }
                })
            }, 2000)
            // save data
        })
        .catch(error => {
            dispatch({
                type: CREATE_CLIENT_ERROR,
                payload: {
                    error: error,
                    loading: false
                }
            })
        })
}


export const updateClient = (dispatch) => (id, data) => {
    
    dispatch({
        type: CREATE_CLIENT,
        payload: {
            data: id,
            loading: true
        }
    })
    
    LModel.update('clients/', id, data)
        .then(response => {

            // Create Client Service handle the response


            setTimeout(() => {
                dispatch({
                    type: UPDATE_CLIENT_SUCCESS,
                    payload: {
                        loading: false,
                        data: response
                    }
                })
            }, 2000)
            // save data
        })
        .catch(error => {
            dispatch({
                type: UPDATE_CLIENT_ERROR,
                payload: {
                    error: error,
                    loading: false
                }
            })
        })
}

export const deleteClient = (dispatch) => (id) => {
    
    dispatch({
        type: DELETE_CLIENT,
        payload: {
            data: id,
            loading: true
        }
    })
    
    LModel.destroy('clients/', id)
        .then(response => {

            // Create Client Service handle the response


            setTimeout(() => {
                dispatch({
                    type: DELETE_CLIENT_SUCCESS,
                    payload: {
                        loading: false,
                        data: response
                    }
                })
            }, 2000)
            // save data
        })
        .catch(error => {
            dispatch({
                type: DELETE_CLIENT_ERROR,
                payload: {
                    error: error,
                    loading: false
                }
            })
        })
}

