import moment from "moment"

// @copyright http://www.mtesfaye.net/date.html
// this date calculation is originally published on the website above
//offset is # days between 1/1/1970 (UNIX time) and the calendar beginning year
//Exampe with 1753 beginning year
//1970-1753=217 years 
// 217 * 365 = 79205 days
// add leap year days between 1753 and 1970 = 52 , 79205 + 52 = 79257
// the remaining 115 days account for the number of days since Meskerem 1 
// to January 1 of 1953 (disregarding the year) to align jan 1 to an 
// Ethiopian date (Tahsas xx).
// It happens that in 1953, Jan 1 = Tahsas 25. That is Meskerem, Tikimt, Hidar 
// with 30 days each equal 90 plus 25 (Tahsas) equals 115
// the final offset equals 79372 = 79257 (from prev calc) + 115 
let OFFSET=79372
let DAY=1000*60*60*24
var EYear
var EMonth
var EDate
var GC
var months="መስከረም,ጥቅምት,ኅዳር,ታኅሣሥ,ጥር,የካቲት,መጋቢት,ሚያዝያ,ግንቦት,ሰኔ,ሐምሌ,ነሐሴ,ጳጉሜ".split(",")

    
export function isValid(dt){
    if (!dt)
        return false
    GC=new Date(dt)
    GC.setHours(0, 0, 0, 0)
    let yearlen=(dt.substr(dt.lastIndexOf("/")+1)).length
    if (yearlen!=4)
        return false
    else return (GC.getFullYear()>=1753)
    }
    
export function getECDays(dt){
    let UTCVal=Date.UTC(GC.getFullYear(),GC.getMonth(),GC.getDate())
    return OFFSET+(UTCVal/DAY)
    }
    
export function ECDate(dt){
    let days=getECDays(dt)
    EYear=1745
    //approximation with errors on EYear %4 = 0
    let years_applied=Math.floor(days/365.25)
    EYear=1745+years_applied
    let days_remaining=days-Math.floor(years_applied*365.25)
    
    //fix the approximation error
    if (EYear%4==0)	  
        days_remaining--

    if (days_remaining==0){
        EYear--
        EMonth=13
        EDate=5 + ((EYear % 4 ==3)?1:0)
        }
    else{
        EMonth = Math.ceil(days_remaining / 30)
        if (days_remaining % 30 ==0)
            EDate = 30
        else EDate=days_remaining % 30
        }
    return EMonth + "/" + (EDate) + "/" + EYear
    }
    
export function getInWords(){
    return months[EMonth-1]+" "+EDate+", "+EYear
    }
export function getECDate(dt){
    isValid(dt)
    return dt ? [ECDate(dt), getInWords()] : ''
}
