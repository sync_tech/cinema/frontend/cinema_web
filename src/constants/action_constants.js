
export const LOGIN_REQUEST = 'auth/login/request'
export const LOGIN_REQUEST_ERROR = 'auth/login/error'
export const LOGIN_REQUEST_SUCCESS = 'auth/login/success'

export const LOGOUT_REQUEST = 'auth/logout/request'
export const LOGOUT_REQUEST_ERROR = 'auth/logout/error'
export const LOGOUT_REQUEST_SUCCESS = 'auth/logout/success'

export const SIGNUP_REQUEST = 'auth/signup/request'
export const SIGNUP_REQUEST_ERROR = 'auth/signup/error'
export const SIGNUP_REQUEST_SUCCESS = 'auth/signup/success'

export const SAVE_TOKEN = 'auth/savetoken'
export const GET_TOKEN = 'auth/gettoken'
export const DELETE_TOKEN = 'auth/deletetoken'

export const RESETPASSWORD_REQUEST = 'auth/request_password/request'
export const RESETPASSWORD_REQUEST_ERROR = 'auth/request_password/error'
export const RESETPASSWORD_REQUEST_SUCCESS = 'auth/request_password/success'

export const CHECK_IS_LOGGEDIN = 'auth/isloggedin'
export const CHECK_IS_LOGGEDIN_SUCCESS = 'auth/isloggedin/success'
export const CHECK_IS_LOGGEDIN_ERROR = 'auth/isloggedin/success'

export const FETCH_LOCATIONS = 'locations/fetch'
export const FETCH_LOCATIONS_SUCCESS = 'locations/fetch/success'
export const FETCH_LOCATIONS_ERROR = 'locations/fetch/error'
export const LOCATIONS_SELECT = 'locations/select'
export const LOCATIONS_SELECT_ERROR = 'locations/select/error'

export const FETCH_LOCATION_AVAILABLE_DESKS = 'locations/desks/fetch'
export const FETCH_LOCATION_AVAILABLE_DESKS_SUCCESS = 'locations/desks/success'
export const FETCH_LOCATION_AVAILABLE_DESKS_ERROR = 'location/desks/error'

export const UPLOAD_FILE = 'global/upload'
export const UPLOAD_FILE_ERROR = 'global/upload/error'
export const UPLOAD_FILE_SUCCESS = 'global/upload/success'
export const SET_PAGE_TITLE = 'global/set-page-title'



export const LIST_CLIENT = 'client'
export const LIST_CLIENT_ERROR = 'client/error'
export const LIST_CLIENT_SUCCESS = 'client/success'

export const CREATE_CLIENT = 'client/new'
export const CREATE_CLIENT_ERROR = 'client/new/error'
export const CREATE_CLIENT_SUCCESS = 'client/new/success'

export const UPDATE_CLIENT = 'client/update'
export const UPDATE_CLIENT_ERROR = 'client/update/error'
export const UPDATE_CLIENT_SUCCESS = 'client/update/success'

export const DELETE_CLIENT = 'client/delete'
export const DELETE_CLIENT_ERROR = 'client/delete/error'
export const DELETE_CLIENT_SUCCESS = 'client/delete/success'


export const CHECK_IS_ROLE_SET = 'user/role'
export const CHECK_IS_ROLE_SET_SUCCESS = 'user/role/set'
export const CHECK_IS_ROLE_SET_ERROR = 'user/role/del'
