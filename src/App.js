import logo from './logo.svg';
import { Router, Switch, Route } from 'react-router-dom'
import { createBrowserHistory } from 'history';
import { AppProvider, useApp } from './providers/app_provider';
import { HistoryProvider } from './providers/history_provider';
import { AuthProvider } from './providers/auth_provider';
import { routes } from "./routes";
import PrivateRoute from "./components/route/private_route";
import './App.css';

const history = createBrowserHistory()

function App() {
  return (

    <AppProvider>
      <HistoryProvider history={history}>
        <AuthProvider>

          <Router history={history}>
            <Switch>
              {
                routes.map((route, index) =>
                  route.private ? <PrivateRoute key={index} {...route} /> : <Route key={index} {...route} />
                )
              }

            </Switch>
          </Router>
        </AuthProvider>
      </HistoryProvider>
    </AppProvider>

  );
}


export default App;
