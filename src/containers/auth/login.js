import React, { useState, useEffect } from 'react'
// import AuthLayout from '../../components/Auth/AuthLayout';
import { Link } from 'react-router-dom'
import { useAuth } from '../../providers/auth_provider';
import { useHistory } from '../../providers/history_provider';
import CustomForm from '../../components/form/custom_form';
import Loading from '../../components/loading/dot_loading'
import AuthService from '../../services/auth_service';
import LModel from "../../services/api";

const Login = (props) => {

  const defaultForm = {
    email: '',
    password: ''
  }

  const [form, setForm] = useState(defaultForm)
  const [remember, setRemember] = useState(false)

  const handleRemeberbox = (e) => {
    setRemember(!remember)
  }

  const handleFormChange = (e) => {
    let newForm = { ...form }
    newForm[e.target.name] = e.target.value
    setForm(newForm)
  }

  const { login, authState, getRole } = useAuth()

  const { history } = useHistory()
  const handleSubmit = (data, errors) => {
    login(data, remember)
  }


  // useEffect(() => {
  //   if (authState.loginSuccess) {
  //     getRoles()
  //     props.history.push('/dashboard')
  //   }
  // }, [authState.loginSuccess, props.history])

  // redirect after login success
  useEffect(() => {
    
    if (authState.loginSuccess && authState.roleSetSuccess) {
      history.push('/dashboard')
    } else if (authState.loginSuccess) {
      getRole()
    }
  }, [authState.loginSuccess, authState.roleSetSuccess, history])


  const fields = [
    {
      name: "username",
      placeholder: "username",
      type: 'text',
      value: "",
      label: "User name",
      rules: {
        required: true
      },
      labelStyle: "text-gray-600",
      divStyle: "my-2"

    },
    {
      name: "password",
      placeholder: "Password",
      type: 'password',
      label: 'Password',
      rules: {
        required: true
      },
      labelStyle: "text-gray-600",
      divStyle: "my-2"
    },
  ]
  return (
    <section className="w-full">
      <h3 className="text-blue-700 text-xl"></h3>

      <div className="pb-4">
        <h1 className="text-3xl font-semibold text-blue-700  text-center">Welcome</h1>
        <p className="text-center text-2xl text-gray-600"> FXTM</p>
        {/* <p className="text-center text-sm text-gray-600">Don't have an account? <Link to="/auth/signup" className="hover:text-teal-800"> Join now </Link></p> */}
        {/* <p className="text-center text-sm text-gray-600">Don't have an account? <Link to="/auth/signup" className="hover:text-teal-800"> Join now </Link></p> */}
      </div>

      {
        authState.error ?
          <p className="text-center text-red-700">Invalid email or password</p>
          : null
      }


      <CustomForm fields={fields} onSubmit={handleSubmit}>
        {/* <input type="checkbox" defaultChecked={remember} onChange={handleRemeberbox} /><span className="text-gray-600 pl-2 text-sm my-2">Remember me</span> */}
        <button type="submit" disabled={authState.loading} className="block w-full bg-blue-700 mt-4 mb-2 p-2 text-white font-bold rounded">{authState.loading ? <Loading loading={authState.loading} /> : 'Login'}
        </button>
      </CustomForm>
      {/* <div className="text-center">
        <p className="text-gray-600 text-sm">Forget password ? <Link to="/auth/forget_password" className="hover:text-teal-800"> Reset here</Link></p>
      </div> */}
    </section>
  )
}

export default Login