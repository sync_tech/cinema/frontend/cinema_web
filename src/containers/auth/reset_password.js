import React, { useState, useEffect } from 'react'
// import AuthLayout from '../../components/Auth/AuthLayout';
import { Link } from 'react-router-dom'
import { useAuth } from '../../providers/auth_provider';
import { useHistory } from '../../providers/history_provider';
import CustomForm from '../../components/form/custom_form';
import Loading from '../../components/loading/dot_loading'


const Login = (props) => {
   
    const defaultForm = {
      email: '',
      password: ''
    }

    const [form, setForm] = useState(defaultForm)
    const [remember, setRemember] = useState(false)

    const handleRemeberbox = (e) => {
      setRemember(!remember)
    }

    const handleFormChange = (e) => {
      let newForm = {...form}
      newForm[e.target.name] = e.target.value
      setForm(newForm)
    }

    const {login, authState} = useAuth()
    const {history} = useHistory()
    const handleSubmit = (data, errors) => {
      // login(data, remember)

      if(data.newpassword === data.confirmpassword && (data.newpassword !== undefined)){
        
        history.push('/my/grid')
      }

    }

    useEffect(() => {
      if(authState.loginSuccess){
        props.history.push('/home')
      }
    }, [authState.loginSuccess, props.history])

    // redirect after login success
    useEffect(() => {
        if(authState.loginSuccess) history.push('/home')
    }, [authState.loginSuccess, history])


    const fields = [
      {
        name: "newpassword",
        placeholder: "new password",
        type: 'password',
        value: "",
        label: "new password",
        rules: {
          required: true
        },
        labelStyle : "text-gray-600 text-sm",
        divStyle: "my-2"

      },
      {
        name: "confirmpassword",
        placeholder: "confirm password",
        type: 'password',
        label:'Confrim password',
        rules: {
          required: true
        },
        labelStyle : "text-gray-600 text-sm",
        divStyle: "my-2"
      },
    ]
    return (
      <section className="w-full">
          <h3 className="text-blue-700 text-xl"></h3>
          {
            authState.error ? 
              <span className="text-red-700">Invalid username or password</span>
            : null
          }
          <div className="pb-2">
        <h1 className="text-2xl font-normal text-blue-700  text-center">Change new Password</h1>
      </div>

          <CustomForm fields={fields} onSubmit={handleSubmit}>

            <button type="submit" disabled={authState.loading} className="block w-full bg-blue-700 mt-4 mb-2 p-2 text-white font-bold rounded">{authState.loading ? <Loading loading={authState.loading}/> : 'Change Password'}</button>
          </CustomForm>
      </section>
    )
}

export default Login