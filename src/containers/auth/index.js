import React from 'react'
import { Route } from 'react-router-dom'
import {authRoutes} from '../../routes'
import AuthLayout from '../../components/auth/auth_layout.js'
import PrivateRoute from '../../components/route/private_route'


const Auth = (props)  => {
        return (
            <AuthLayout isLoading={props.loading}>
            <div className="self-center p-10 rounded sm:w-auto md:w-3/4 lg:w-2/3 xl:w-1/3 bg-white border-4 border-indigo-300">
                {/* <h3 className="font-light text-xl leading-tight text-blue-700">FXTM</h3> */}
                {/* <hr className="w-1/3 border-gray-500 mt-4 mb-8 border-2" /> */}

                {
                authRoutes.map((route, index) =>
                  route.private ? <PrivateRoute key={index} {...route} /> : <Route key={index} {...route} />
                )
              }

              </div>
            </AuthLayout>
        )
}

export default Auth