import React from 'react';

export default () => {

    return(
        <div className="bg-red-300">
        <h1 className="text-2lg"> Page Not Found </h1>
        </div>
    )
};