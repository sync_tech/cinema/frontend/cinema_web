import React, { useReducer, useContext, useMemo, createContext, useEffect } from 'react'
import authReducer, { initialState } from '../reducers/auth_reducer'
import { login as remoteLogin, logout as remoteLogout, signup as remoteSignup, reset_password as remoteForgetPassword, getRole as checkIsRoleSet} from '../actions/auth_actions'
import { LOGOUT_REQUEST_SUCCESS, LOGOUT_REQUEST_ERROR } from '../constants/action_constants';
export const AuthContext = createContext()

export const AuthProvider = (props) => {
    const [state, dispatch] = useReducer(authReducer, initialState)
    const value = useMemo(() => [state, dispatch], [state])
    return <AuthContext.Provider value={value} {...props} />
}

export const useAuth = () => {
    const context = useContext(AuthContext)
    if(!context){
        throw new Error('useAuth must be used within a AuthProvider')
    }

    const [authState, dispatch] = context

    const login = (data, remember) => remoteLogin(dispatch)(data, remember)
    // const register = (data) => remoteRegister(dispatch)(data)
    const logout = () => authState.isLoggedIn ? remoteLogout(dispatch)() : dispatch({
        type: LOGOUT_REQUEST_SUCCESS
    })

    const getRole = () => authState.isLoggedIn ? checkIsRoleSet(dispatch)() : null

    const signup = (data) => remoteSignup(dispatch)(data)

    const reset_password = (data) => remoteForgetPassword(dispatch)(data)
    

    return {
        authState,
        dispatch,
        login,
        logout,
        signup,
        reset_password,
        getRole
    }
}