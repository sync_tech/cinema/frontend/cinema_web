import React, { useState, useEffect, Fragment } from 'react'
import { FolderPlus, Search } from 'react-feather';
import { useHistory } from '../../providers/history_provider';
import CircularLoading from '../loading/circular_loading'
import LModel from '../../services/api'



export default ({ handleAddingToCard, reloadPage }) => {


    const [dotloading, setDotLoading] = useState(false)

    const [searchKey, setSearchKey] = useState("")
    const [circularloading, setCircularloading] = useState(false)
    const [searchResult, setSearchResult] = useState()
    const [products, setProducts] = useState([])
    const { history } = useHistory()


    const loadProduct = () => {
        setDotLoading(true)
        // LModel.find('products/')
        LModel.find(`client/?search=`)
            .then(res => {
                // console.log(res)
                setProducts(res)
                setDotLoading(false)
            }).catch(err => {
                console.log(err)
                setDotLoading(false)
            })
    }

    useEffect(() => {
        loadProduct()
    }, [reloadPage])

    const handlingSearchInputChange = (e) => {

        if (e.target.value != null && e.target.value.trim().length > 1) {
            setSearchKey(e.target.value)
            // loadSearchResult()
        } else if (e.target.value === "") {
            setSearchKey("")
            // setCircularloading] = useState(false)
            setSearchResult()
        } else {
            setSearchKey(e.target.value.trim())
        }


    }

    const handlingSearchButtonClick = () => {

        if (searchKey != null && searchKey.length > 1) {
            loadSearchResult()
        }
    }


    const loadSearchResult = () => {
        setCircularloading(true)
        LModel.find(`client/?search=${searchKey}`)
            .then(res => {
                // if(res.results){
                setSearchResult(res)
                setProducts(res)
                setCircularloading(false)
                // }
            }).catch(err => {
                console.log(err)
                setCircularloading(false)
            })

    }


    useEffect(() => {
        if (searchKey != null && searchKey.trim().length > 1) {
            loadSearchResult()
        } else {
            loadProduct()
        }
    }, [searchKey])



    return (

        <Fragment>

            <div className="w-5/12">
                <div className="text-center relative mx-auto text-gray-600">
                    <input className="w-full relative border-2 border-gray-300 bg-white h-12 px-5 pr-16 rounded-lg text-gray-700 focus:outline-none"
                        autoComplete="off" type="search" name="search" placeholder="Search client "
                        onChange={handlingSearchInputChange}
                        value={searchKey} />
                    <button type="submit" onClick={handlingSearchButtonClick} className="absolute right-0 top-0 mt-3 mr-4">
                        {
                            circularloading ?
                                <CircularLoading loading={circularloading} color={"bg-gray-600"} />
                                :
                                <Search size={20} strokeWidth={3} />
                        }
                    </button>
                </div>
            </div>

            {

                dotloading ?
                    <div className="p-4">
                        <CircularLoading loading={dotloading} color={"bg-gray-600"} />
                    </div>
                    :

                    //  ?
                    products.length > 0 || searchResult != null && searchResult != undefined && searchResult.length > 0 ?
                        <div className="grid3 m-2 py-4 overflow-x-auto" style={{ maxHeight: '84vh', minHeight: 'auto' }}>
                            {
                                products.map((v, index) => (
                                    <div key={index} onClick={() => handleAddingToCard(v)} className="max-w-sm rounded border border-blue-200 shadow-md hover:shadow-lg m-2">
                                        <div className="text-center p-2">
                                            <p className="text-sm">{v.name}</p>
                                        </div>

                                        <div className="text-center p-2">
                                            <span className="inline-block bg-gray-100 rounded-full px-3 py-1 text-sm text-gray-900">{v.quantity}</span>
                                        </div>
                                    </div>
                                ))
                            }
                        </div>

                        :
                        // <div className="w-full text-center p-4">

                        //     <div className="">
                        //         <p className={searchResult === undefined ? 'hidden' : 'block text-red-600 text-2xl font-semibold'}> {`Not Found ${searchResult}`}</p>
                        //         <p>
                        //             <FolderPlus onClick={() => history.push('/product')} className="inline-flex text-center rounded-lg m-2 hover:bg-purple-100" size={140} color={'gray'} strokeWidth={0.5} />
                        //         </p>
                        //         <em>Your product list is Empty.</em>
                        //     </div>


                        // </div>
                        null

            }


        </Fragment>
    )



}








