import React from 'react'

export default ({ count, previous, next, list, activeIndex, setIndex }) => {

    // get next page from next string 
    // set it to active number if next is valid

    // if (next) {
    //     let nx = next.lastIndexOf("&page")
    //     let num = next.substring(nx + 6)
    //     let indx = parseInt(num) - 1
    //     activeIndex = indx
    //     console.log(indx)
    // }

    const GenerateList = () => {
        let page = Math.ceil(count / list)
        let tableList = [];

        for (let i = 1; i <= page; i++) {

            if (activeIndex === i) {
                tableList.push(
                    <li key={i} onClick={() => setIndex(i)} className="mx-1 px-3 py-2 bg-blue-600 text-white hover:bg-blue-500 hover:text-gray-200 rounded-lg shadow-md border border-gray-500">
                        <div className="font-bold">{i}</div>
                    </li>)
            } else {

                tableList.push(
                    <li key={i} onClick={() => setIndex(i)} className="mx-1 px-3 py-2 bg-white text-gray-700 hover:bg-blue-500 hover:text-gray-200 rounded-lg shadow-md border border-gray-500">
                        <div className="font-bold">{i}</div>
                    </li>)
            }

            //     if (i === props.cpage) {
            //         tableList.push(<li className="page-item active" key={i} ><Link className="page-link" to="#">{i}</Link></li>)
            //     } else {
            //         tableList.push(<li className="page-item" key={i} onClick={() => {
            //             props.pageClick(i, ((i - 1) * props.limit))
            //             props.pageloading(false)
            //         }}><Link className="page-link" to="#" id={i}>{i}</Link></li>)
            //     }

        }

        return (
            tableList
        )


    }

    return (
        count > 0 ?
            <div className="w-full p-6">
                <ul className="flex justify-center">
                    <li className={`mx-1 px-3 py-2 bg-white rounded-lg shadow-md border border-gray-500 ${previous != null ? 'text-gray-700 hover:bg-blue-400 hover:text-gray-200' : 'text-gray-500'} `}
                        onClick={() => {
                            if (previous) {
                                setIndex(activeIndex - 1)
                            }
                        }}>
                        <div className="flex items-center font-bold">
                            <span className="mx-1">Previous</span>
                        </div>
                    </li>

                    {
                        GenerateList()
                    }
                    {/* <li className="mx-1 px-3 py-2 bg-gray-200 text-gray-700 hover:bg-gray-700 hover:text-gray-200 rounded-lg">
                    <a className="font-bold" href="#">1</a>
                </li>
                <li className="mx-1 px-3 py-2 bg-gray-200 text-gray-700 hover:bg-gray-700 hover:text-gray-200 rounded-lg">
                    <a className="font-bold" href="#">2</a>
                </li>
                <li className="mx-1 px-3 py-2 bg-gray-200 text-gray-700 hover:bg-gray-700 hover:text-gray-200 rounded-lg">
                    <a className="font-bold" href="#">3</a>
                </li> */}

                    <li className={`mx-1 px-3 py-2 bg-white  rounded-lg shadow-md border border-gray-500 ${next != null ? 'text-gray-700 hover:bg-blue-400 hover:text-gray-200' : 'text-gray-500'}`}
                        onClick={() => {
                            if (next) {
                                setIndex(activeIndex + 1)
                            }
                        }}>
                        <div className="flex items-center font-bold">
                            <span className="mx-1">Next</span>
                        </div>
                    </li>
                </ul>
            </div>
            :
            null
    )

}