import React from 'react'

const AuthLayout = (props) => (
    
    <div className="min-h-screen w-screen flex justify-center bg-gray-200">
        {props.children}   
    </div>
                    
)

export default AuthLayout
