import React, { useState, Fragment, useEffect } from 'react'
import moment from 'moment'
/**
 * Custom bulma field
 * @param {error, placeholder, name, onChange, styles, required, type} props - 
 */
export default ({error, placeholder, name, value, onBlur, onChange, type, label, rules, hasErrorCallback, defaultValue, options, valueKey, labelKey, horizontal=false,selectStyle="",inputStyle="",labelStyle = "",divStyle="",}) => {
    

    const [validationError, setValidationError] = useState(error ? true : false)
    const [validationErrorMessage, setValidationErrorMessage] = useState(error)
    const [internalValue, setInternalValue] = useState(defaultValue)
    const [dirty, setDirty] = useState(false)
    const [dateDescription, setDateDescription] = useState()

    useEffect(() => {
        onChange(internalValue)
        if (rules && dirty) applyValidators()
        setDirty(true)
    }, [internalValue])

    useEffect(() => {
        if(dirty && typeof(hasErrorCallback) === 'function') hasErrorCallback(validationError)
    }, [validationError])

    const requiredValidator = (message) => {
        let msg = typeof(message) === 'string' && message ? message : 'This field is required'
        msg = !internalValue ? msg : error
        if(msg){
            setValidationErrorMessage(msg)
        }
        setValidationError(msg ? true : false)
    }

    const minValidator = (val) => {
        const isObject = typeof(val) === 'object'
        let message = isObject ? val.message : 'less than minimum value ' + (isObject ? val.value : val) + ' given'
        message = isObject && internalValue < val.value || !isObject && internalValue < val ? message : error
        if(message) {
            setValidationErrorMessage(message)
        }
        setValidationError(message ? true : false)
    }

    const maxValidator = (val) => {
        const isObject = typeof(val) === 'object'
        let message = isObject ? val.message : 'greater than maximum value ' + (isObject ? val.value : val) + ' given'
        message = isObject && internalValue > val.value || !isObject && internalValue > val ? message : error
        if(message){
            console.log('> max validator ', message)
            setValidationErrorMessage(message)
        }
        setValidationError(message ? true : false)
    }

    const applyCustomValidator = (customValidator) => {
        const isFunction = typeof(customValidator) === 'function'
        if(!isFunction) throw 'given custom validator is not a function'
        const customValidationError = customValidator(internalValue)
        setValidationError(customValidationError ? true : false)
    }

    const availableValidators = {
        'required': msg => requiredValidator(msg),
        'min': val => minValidator(val),
        'max': val => maxValidator(val),
        'custom': custom => applyCustomValidator(custom)
    }

    const applyValidators = () => {
        if(typeof(rules) !== 'object')
            throw 'rules must be object of {<rule-name>: value}'
        
        Object.keys(rules).forEach(rule => {
            // console.log('rule', rule)
            const validator = Object.keys(availableValidators).includes(rule) ? 
                availableValidators[rule] 
                : 
                () => {throw 'validation key not found. available options are (required, min, max, custom)'}
            validator(rules[rule])
            
        })
        applyOnBlur()
    }

    const applyOnBlur = () => {
        if(typeof(onBlur) === 'function')
            onBlur({
                error: validationError,
                value: internalValue
            })
        else 
            throw 'onBlur is not a function'
    }

    const renderSelect = () => {
        return (
            <select 
                name={name}
                onBlur={() => rules ? applyValidators() : applyOnBlur()}
                defaultValue={defaultValue} onChange={
                e => {
                    setInternalValue(e.target.value)
                }
            } className={`w-full pt-2 pb-2 rounded bg-transparent border ${selectStyle?selectStyle:""} ${validationError? 'border-red-500' : 'border-blue-800'}`}>
                <option> --- Select {label} ---</option>
                {
                    options.map(option => (
                        <option key={option[valueKey]} value={option[valueKey]}>{option[labelKey]}</option>
                    ))
                }
            </select>
        ) 
    }

    const renderTextArea = () => {
        return (
            <textarea name={name} placeholder={placeholder} 
                onBlur={() => rules ? applyValidators() : applyOnBlur()}
                className={`border w-full p-2 rounded ${validationError? 'border-red-500' : 'border-blue-800'}`}
                rowSpan={20} defaultValue={defaultValue} onChange={e => {
                setInternalValue(e.target.value)
            }}/>
        )
    }

    const renderOthers = () => {
        return (
            <Fragment>
            <input className={`w-full p-2 bg-transparent  border ${validationError ? 'border-red-500' : 'border-blue-800'} ${inputStyle}`}
                type={type} placeholder={placeholder}
                name={name}
                onChange={e => {
                    setInternalValue(e.target.value)
                }}
                defaultValue={defaultValue}
                onBlur={() => rules ? applyValidators() : applyOnBlur()}
                autoComplete="off"
                />
                {
                    type === 'date' && internalValue ?
                        <span className="italic text-gray-500">{moment(internalValue).format('l')}</span>
                    : null
                }
            </Fragment>
        )

    }

    const renderInput = () => {
        switch (type) {
            case 'select':
                return renderSelect()
            case 'textarea':
                return renderTextArea()
            default:
                return renderOthers();
        }
    }
    
    return (
        <div className={`${divStyle ? divStyle : 'w-full' }  ${horizontal ? 'flex flex-row px-1' : ''}`} >
            {
                label ?
                <label className={`${labelStyle} self-center`} htmlFor={name} >{label}</label>
                :null
            }
            <div className="py-1 w-full">
                {renderInput()}
            <span className={`${!validationError ? 'hidden' : ''} text-red-500`}>{validationErrorMessage}</span>                        
            </div>
        </div>
    )    
}