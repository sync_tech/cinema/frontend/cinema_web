// import React, { Component } from 'react'
// import CustomField from './custom_field';
// import { validator } from '../../services/form_service';

// /**
//  * Custom form for bulma field components
//  * @param {fields, onSubmit} props 
//  * @todo not validating
//  */
// class CustomForm extends Component{
//     constructor(props){
//         super(props)
        
//         this.state = {
//             formData: {},
//             formErrors: {}
//         }
//     }

//     componentDidMount(){
//         let formData = {}
//         let formErrors = {}
//         this.props.fields.map(field => {
//             formData[field.name] = field.value ? field.value : ''
//             formErrors[field.name] = field.error ? field.error : ''
//             // console.log(field, formData, formErrors)
//         })

//         this.setState({
//             formData: formData,
//             formErrors: formErrors
//         }, () => {
//         })
//     }

//     handleFormChange = (name, value) => {
//         let form = {...this.state.formData}
//         form[name] = value
//         this.setState({
//           formData: form, 
//         }, () => {
//             if (typeof(this.props.onChange) === 'function')
//                 this.props.onChange(this.state.formData)
//         })
//       }

//     handleSubmit = (e) => {
//         // console.log('> submitting')
//         e.preventDefault()
//         this.props.onSubmit(this.state.formData, this.state.formErrors)
//     }

//     render(){
//         return(
//             <form onSubmit={this.handleSubmit} className="w-full">
//                 {
//                     this.props.fields.map((field, index) => {
//                         return Array.isArray(field) ? 
//                                 <div className="flex w-full" key={index}>
//                                     {field.map((innerfield, innerIndex )=> (
//                                         <div className="flex-1 mr-2" key={innerIndex} >
//                                             <CustomField {...innerfield}
//                                             defaultValue={innerfield.value}
//                                             error={this.state.formErrors[innerfield.name] ? this.state.formErrors[innerfield.name].error : ''} 
//                                             onChange={value => this.handleFormChange(innerfield.name, value)}
//                                             onBlur={() => this.setState({formErrors: validator(innerfield, this.state.formData, {...this.state.formErrors})})}
//                                             rules={innerfield.rules}
//                                             inputStyle="bg-white rounded"
//                                             selectStyle="bg-white"
//                                             />
//                                         </div>
//                                     ))}

//                                 </div>
//                                 : 
//                                 <CustomField {...field} key={index}
//                                     defaultValue={field.value}
//                                     error={this.state.formErrors[field.name] ? this.state.formErrors[field.name].error : ''} 
//                                     rules={field.rules}
//                                     onChange={(value) => this.handleFormChange(field.name, value)}
//                                     onBlur={() => this.setState({formErrors: validator(field, this.state.formData, {...this.state.formErrors})})}
//                                     inputStyle="bg-white rounded"
//                                     selectStyle="bg-white"
//                                     />
//                     })
//                 }
                
//                 {
//                     this.props.children
//                 }
//             </form>
//         )
//     }
// }

// export default CustomForm